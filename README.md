## Tourist API / Back-End

Below are the api endpoints.

/weather
- this is used to get the weather information of a queried city from japan.

parameters: 

1. y (string city).
2. z (string zone) default value = jp.

/place_info
- this is used to get places for tourist attaction nearby on the queried city from japan.

parameters: 

1. y (string city).
2. z (string zone) default value = jp.

---

## Tourist APP / Front-End

The UI / UX implemented on this Tourist App was simple and easy to use that fit for most of the travelers. When the web app is fully loaded, then it is ready to use by searching desired city and then hit enter. It is the fastest way in providing a good service for the travelers to display the place information and the details of their desired city in japan.

Also the link that redirect to search engine is added as feature for getting more details about the place.

